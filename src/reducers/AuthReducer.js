import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,
  NAME_CHANGED,
  FETCH_KEYS_SUCCESS,
  FETCH_KEYS_FAILURE
} from '../actions/types';

const INITIAL_STATE = {
  name: '',
  email: '',
  password: '',
  user: null,
  error: '',
  loading: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case EMAIL_CHANGED:
      return { ...state, email: action.payload };
    case NAME_CHANGED:
      return { ...state, name: action.payload };
    case PASSWORD_CHANGED:
      return { ...state, password: action.payload };  
    case LOGIN_USER:
      return { ...state, loading: true, error: '' };
    case LOGIN_USER_SUCCESS:
      return { ...state, ...INITIAL_STATE, user: action.payload };
    case LOGIN_USER_FAIL:
      return { ...state, error: 'Authentication Failed.', password: '', loading: false };     
    case FETCH_KEYS_SUCCESS:
        return { ...state, apikey: action.payload };
    case FETCH_KEYS_FAILURE:
        return { ...state, apikey: '', error: 'failed to get key' };          
    default:
      return state;
  }
};
