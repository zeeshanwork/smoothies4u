import {
  CARDNUMBER_CHANGED,
  EXPIRYMONTH_CHANGED,
  EXPIRYYEAR_CHANGED,
  CVV_CHANGED,
  FETCH_PAYMENT_INFO_SUCCESS,
  FETCH_PAYMENT_INFO_FAILURE,
  GENERATE_CARDTOKEN_SUCCESS,
  GENERATE_CARDTOKEN_FAILURE,
  MAKE_PAYMENT,
  MAKE_PAYMENT_SUCCESS,
  MAKE_PAYMENT_FAILURE
} from '../actions/types';

const INITIAL_STATE = {
  error: '',
  loading: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {  
      
    case CARDNUMBER_CHANGED:
        return { ...state, cardnumber: action.payload };
    case EXPIRYMONTH_CHANGED:
        return { ...state, expmonth: action.payload };
    case EXPIRYYEAR_CHANGED:
        return { ...state, expyear: action.payload };
    case CVV_CHANGED:
        return { ...state, cvv: action.payload };
    case GENERATE_CARDTOKEN_SUCCESS:
          return { ...state,  token: action.payload.token, 
                              cardid: action.payload.cardid,
                              lastdigits: action.payload.lastdigits,
                              loading: false};
    case MAKE_PAYMENT: 
          return { ...state,  loading: true, error: ''};      
    case MAKE_PAYMENT_SUCCESS:
          return { ...state,  ordercompleted: true, error: '', loading: false};
    case MAKE_PAYMENT_FAILURE:
          return { ...state,  ordercompleted: false, error: 'Card Transaction failed', loading: false};
    case GENERATE_CARDTOKEN_FAILURE:
        return { ...state, keys: '', error: 'failed to get token' };     
    case FETCH_PAYMENT_INFO_SUCCESS:      
      return { ...state,  paymentinfo: action.payload, loading: false };
    case FETCH_PAYMENT_INFO_FAILURE:
      return { ...state,  error: 'Failed to retrieve data.', 
                          paymentinfo: '', 
                          loading: false };      
    default:
      return state;
  }
};
