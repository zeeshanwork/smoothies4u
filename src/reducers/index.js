import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import ProductReducer from './ProductReducer';
import CardReducer from './CardReducer';

export default combineReducers({
  auth: AuthReducer,
  product: ProductReducer,
  card: CardReducer
});
