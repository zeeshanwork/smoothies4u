import {
  FETCH_PRODUCTS_SUCCESS,
  FETCH_PRODUCT_BY_ID_SUCCESS,
  QUANTITY_CHANGED
} from '../actions/types';

const INITIAL_STATE = { list: [], item: {} };

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
      case QUANTITY_CHANGED:
          return { ...state, quantity: action.payload };
      case FETCH_PRODUCTS_SUCCESS:
          return { ...state, list: action.payload };
      case FETCH_PRODUCT_BY_ID_SUCCESS:
          return { ...state, item: action.payload };
      default:
          return state;
    }
};
