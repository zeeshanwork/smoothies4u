import axios from 'axios';
import { Actions } from 'react-native-router-flux';

import {
  NAME_CHANGED,
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,
  SIGNUP_USER,
  FETCH_KEYS_SUCCESS,
  FETCH_KEYS_FAILURE,
  ROOT_URL
} from './types';

export const nameChanged = (text) => {
  return {
    type: NAME_CHANGED,
    payload: text
  };
};

export const emailChanged = (text) => {
  return {
    type: EMAIL_CHANGED,
    payload: text
  };
};

export const passwordChanged = (text) => {
  return {
    type: PASSWORD_CHANGED,
    payload: text
  };
};

export function getKey() {
 return function (dispatch) {
    axios.get(`${ROOT_URL}/getKey`)
      .then(response => {
        dispatch({
          type: FETCH_KEYS_SUCCESS,
          payload: response.data.key
        });
      }
      ).catch((response) => {
        dispatch({
          type: FETCH_KEYS_FAILURE,
          payload: response.data.error
        });
      });
    }; 
}

export const signupUser = ({ name, email, password }) => {
  return (dispatch) => {
    dispatch({ type: SIGNUP_USER });

    // Submit email/password to the server
    axios.post(`${ROOT_URL}/signUp`, { name, email, password })
      .then(response => {
        // If request is good...
        // - Update state to indicate user is authenticated
        loginUserSuccess(dispatch, response);
      })
      .catch(() => {
        // If request is bad...
        // - Show an error to the user
        loginUserFail(dispatch);
      });
  };
};


export const loginUser = ({ email, password }) => {
  return (dispatch) => {
    dispatch({ type: LOGIN_USER });

    // Submit email/password to the server
    axios.post(`${ROOT_URL}/signIn`, { email, password })
      .then(response => {
        // If request is good...
        // - Update state to indicate user is authenticated        
        loginUserSuccess(dispatch, response);
      })
      .catch(() => {
        // If request is bad...
        // - Show an error to the user
        loginUserFail(dispatch);
      });
  };
};

const loginUserFail = (dispatch) => {
  dispatch({ type: LOGIN_USER_FAIL });
};

const loginUserSuccess = (dispatch, user) => {
  dispatch({
    type: LOGIN_USER_SUCCESS,
    payload: user
  });  
  Actions.main();
};
