import axios from 'axios';
import { Actions } from 'react-native-router-flux';
import Stripe from 'react-native-stripe-api';

import {
  FETCH_PAYMENT_INFO_SUCCESS,
  FETCH_PAYMENT_INFO_FAILURE,
  GENERATE_CARDTOKEN_SUCCESS,
  GENERATE_CARDTOKEN_FAILURE,
  FETCH_CUSTOMER_CARDINFO_SUCCESS,
  FETCH_CUSTOMER_CARDINFO_FAILURE,
  MAKE_PAYMENT,
  MAKE_PAYMENT_SUCCESS,
  MAKE_PAYMENT_FAILURE,
  CARDNUMBER_CHANGED,
  EXPIRYMONTH_CHANGED,
  EXPIRYYEAR_CHANGED,
  CVV_CHANGED,
  ROOT_URL
} from './types';

export function fetchUserPaymentInfo(username) {
 return function (dispatch) {
    axios.get(`${ROOT_URL}/getUsersPaymentInfo?username=${username}`)
      .then(response => {
        dispatch({
          type: FETCH_PAYMENT_INFO_SUCCESS,
          payload: response.data.item
        });
      }
      ).catch((response) => {
        dispatch({
          type: FETCH_PAYMENT_INFO_FAILURE
        });
      });
    }; 
}

export const cardnumberChanged = (text) => {
  return {
    type: CARDNUMBER_CHANGED,
    payload: text
  };
};

export const expmonthChanged = (text) => {
  return {
    type: EXPIRYMONTH_CHANGED,
    payload: text
  };
};

export const expyearChanged = (text) => {
  return {
    type: EXPIRYYEAR_CHANGED,
    payload: text
  };
};

export const cvvChanged = (text) => {
  return {
    type: CVV_CHANGED,
    payload: text
  };
};

export function generatePaymentToken(postObj) {
 return function (dispatch) {  

   dispatch({ type: MAKE_PAYMENT });

   const {  apikey, username, cardnumber, 
            expmonth, expyear, cvv,
            stripe_cust_id, stripe_card_id, 
            stripe_token_id, last4 } = postObj;
   
  const client = new Stripe(apikey);
  client.createToken(cardnumber , expmonth, expyear, cvv)
     .then(token => {
     console.log(token);
         client.createCustomer(token.id, username)
         .then(customer => {
           console.log(customer);

                            axios.post(`${ROOT_URL}/updateUserPaymentInfo`,  
                                       { username, customer: customer.id, token: token.id, card: token.card.id, last4: token.card.last4 })
                              .then(response => {
                                 Actions.order({ message: `Your card is successfully added to the system . Thank you.`});
                              }
                              ).catch((response) => {
                              console.log(response);
                                dispatch({
                                  type: GENERATE_CARDTOKEN_SUCCESS_FAILURE,
                                  payload: response.data.error
                                });
                              });
          }).catch((response) => {
             console.log(response);
              dispatch({
                type: GENERATE_CARDTOKEN_SUCCESS_FAILURE,
                payload: response.data.error
              });
            });
 
    }
    ).catch((response) => {
    
      console.log(response);
      dispatch({
        type: GENERATE_CARDTOKEN_FAILURE,
        payload: response.data.error
      });
    });
 }
}

export function makeOrderPayment(postObj) {
 return function (dispatch) {  

   dispatch({ type: MAKE_PAYMENT });
   
   const {  apikey, username, quantity, amount, name, stripe_cust_id } = postObj;
   const client = new Stripe(apikey);
   
   client.createCharge(amount * 100, stripe_cust_id, `${quantity} packs of ${name}`,'USD')
     .then(response => {
            if(response.status === "succeeded")
            {
                Actions.order({ message: `Your order of ${quantity} packs of ${name} is successfully processed. Thank you.`});
            }
            else
            {
                Actions.order({ message: `Your order could not be processed. Please try again later.`});
            } 
    }).catch((response) => {    
      console.log(response);
      dispatch({
        type: MAKE_PAYMENT_FAILURE,
        payload: response.data.error
      });
    });
 }
}


