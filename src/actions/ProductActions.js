import axios from 'axios';
import AsyncStorage from 'react-native';
import {
  FETCH_PRODUCTS_SUCCESS,
  FETCH_PRODUCTS_FAILURE,
  FETCH_PRODUCT_BY_ID_SUCCESS,
  FETCH_PRODUCT_BY_ID_FAILURE,
  QUANTITY_CHANGED,
  ROOT_URL
} from './types';
import { Actions } from 'react-native-router-flux';

export function productsFetch() {
 return function (dispatch) {
    axios.get(`${ROOT_URL}/getProductsList`)
      .then(response => {
        dispatch({
          type: FETCH_PRODUCTS_SUCCESS,
          payload: response.data.list
        });
      }
      ).catch((response) => {
        // If request is bad...
        // - Show an error to the user
        dispatch({
          type: FETCH_PRODUCTS_FAILURE,
          payload: response.data.error
        });
      });
    }; 
}

export const quantityChanged = (text) => {
  return {
    type: QUANTITY_CHANGED,
    payload: text
  };
};

export function goBacktoProductsList(postObj) {
 return function (dispatch) {     
  Actions.productsList({});
 }
};


