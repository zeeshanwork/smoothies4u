import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = ({ onPress, children }) => {
  const { buttonStyle, textStyle } = styles;

  return (
    <TouchableOpacity onPress={onPress} style={buttonStyle}>
      <Text style={textStyle}>
        {children}
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
  textStyle: {
    alignSelf: 'center',
    color: '#FEF',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 6,
    paddingBottom: 6
  },
  buttonStyle: {
    flex: 1,
    backgroundColor: '#3B5998',
    marginTop: 5,
    marginBottom: 10,
    borderRadius: 2,
    borderWidth: 1,
    marginLeft: 6,
    marginRight: 6
  }
};

export { Button };
