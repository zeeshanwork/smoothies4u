import React, { Component } from 'react';
import { View, Text, Image, TouchableWithoutFeedback } from 'react-native';
import { Card, CardSection, Button } from './common';
import { goBacktoProductsList } from '../actions';
import { connect } from 'react-redux';

class Order extends Component {

onRowPress() {
  this.props.goBacktoProductsList();
}

  render() {
    const { message } = this.props;
    const { headerContentStyle,			
            thumbnailContainerStyle,
            headerTextStyle,
            imageStyle,
            ratingStyle
          } = styles;
    
    return (
      <TouchableWithoutFeedback onPress={this.onRowPress.bind(this)} >
        <Card >
           <View style={headerContentStyle}>
                <Text style={headerTextStyle} numberOfLines={4}
                > {message} </Text>
              </View>
          <CardSection> 
            <Button onPress={this.onRowPress.bind(this)} >
                Smoothies List
              </Button>
          </CardSection>
      </Card>
    </TouchableWithoutFeedback>
    );
  }
}

const styles = {
  titleStyle: {
    fontSize: 22,
    fontWeight: 'bold',
    paddingLeft: 15,
    justifyContent: 'center',
		alignItems: 'center',
  },
	headerContentStyle: {
		flexDirection: 'column',
		justifyContent: 'center',
	},
	imageStyle: {
		height: 160,
		flex: 1,
		width: null
	},
	thumbnailContainerStyle: {
		justifyContent: 'center',
		alignItems: 'center',
		marginLeft: 10,
		marginRight: 10,
        padding: 10
	},
	headerTextStyle: {
		fontSize: 18,
        fontWeight: 'bold',
        color: "rgba(106,174,25,1)",
        textAlign: 'center',
        paddingLeft:10
	},
    ratingStyle: {
    color: "rgba(234,61,17,1)"
  }
};

export default connect(null, {
  goBacktoProductsList
})(Order);
