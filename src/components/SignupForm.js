import React, { Component } from 'react';
import { View, Text, Image, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { nameChanged, emailChanged, passwordChanged, signupUser } from '../actions';
import { Card, CardSection, Input, Button, Spinner } from './common';

class SignupForm extends Component {
  onNameChange(text) {
    this.props.nameChanged(text);
  }

  onEmailChange(text) {
    this.props.emailChanged(text);
  }

  onPasswordChange(text) {
    this.props.passwordChanged(text);
  }

  onButtonPress() {
    const { name, email, password } = this.props;
    AsyncStorage.setItem('username', email); 
    this.props.signupUser({ name, email, password });
  }

  renderButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button onPress={this.onButtonPress.bind(this)}>
        Sign up
      </Button>
    );
  }

  render() {
    return (
      <View style={styles.backgroundColor} >
       <Card>
          <CardSection style={styles.thumbnailContainerStyle}> 
            <Image 
            style={styles.imageStyle}
             source={ require('../assets/smoothiesgrid.jpg')} 
            />
        </CardSection>
      </Card>

      <Card>
        <CardSection>
          <Input
            label="Full Name"
            placeholder="your name"
            onChangeText={this.onNameChange.bind(this)}
            value={this.props.name}
          />
        </CardSection>

        <CardSection>
          <Input
            label="Email"
            placeholder="email@gmail.com"
            onChangeText={this.onEmailChange.bind(this)}
            value={this.props.email}
          />
        </CardSection>

        <CardSection>
          <Input
            secureTextEntry
            label="Password"
            placeholder="password"
            onChangeText={this.onPasswordChange.bind(this)}
            value={this.props.password}
          />
        </CardSection>

        <Text style={styles.errorTextStyle}>
          {this.props.error}
        </Text>

        <CardSection>
          {this.renderButton()}
        </CardSection>
      </Card>
      </View>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 18,
    alignSelf: 'center',
    color: "rgba(255,33,0,1)"
  },
  backgroundColor: {
    backgroundColor: '#EEE', 
    flex: 0,
    height: 200
  },
  thumbnailContainerStyle: {
      justifyContent: 'center',
      alignItems: 'center',

  },
  imageStyle: {
      height: 300,
      flex: 1,
      width: null
  }
};

const mapStateToProps = ({ auth }) => {
  const { name, email, password, error, loading } = auth;

  return { name, email, password, error, loading };
};

export default connect(mapStateToProps, {
 nameChanged, emailChanged, passwordChanged, signupUser
})(SignupForm);
