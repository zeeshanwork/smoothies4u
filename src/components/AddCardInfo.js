import React, { Component } from 'react';
import { View, Text, Image, Picker, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { cardnumberChanged, expmonthChanged, 
         expyearChanged, cvvChanged, 
         generatePaymentToken, getKey
       } from '../actions';
import { Card, CardSection, Input, Button, Spinner } from './common';

class AddCardForm extends Component {
  componentDidMount(){     
    this.props.getKey();       
  }
  
  onCardNumberChange(text) {
    this.props.cardnumberChanged(text);
  }

  onExpiryMonthChange(text) {
    this.props.expmonthChanged(text);
  }

  onExpiryYearChange(text) {
    this.props.expyearChanged(text);
  }

  onCVVChange(text) {
    this.props.cvvChanged(text);
  }

  onButtonPress() {
    const { apikey, cardnumber, expmonth, expyear, cvv } = this.props;
    const {stripe_cust_id, stripe_card_id, stripe_token_id, last4} = this.props.paymentinfo[0];

    AsyncStorage.getItem('username', (err, username) => {
      this.props.generatePaymentToken({
                                        apikey, username, cardnumber, 
                                        expmonth, expyear, cvv,
                                        stripe_cust_id, stripe_card_id, 
                                        stripe_token_id, last4
                                      });
    });  
  }

  renderButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button onPress={this.onButtonPress.bind(this)}>
        Update Card Information
      </Button>
    );
  }

  render() {
    return (
      <View style={styles.backgroundColor} >
       
      <Card>
        <CardSection>
          <Input
            label="Card Number"
            placeholder="XXXXXXXXXXXXXXXX"
            onChangeText={this.onCardNumberChange.bind(this)}
            value={this.props.cardnumber}
            maxLength={16}
          />
        </CardSection>

        <CardSection>
           <Text style={{fontSize:16, marginLeft:14}}> Expiry Month / Year </Text>
          <Picker 
            style={{
              width: 100,
            }}
            selectedValue={(this.props && this.props.expmonth) || '1'}
            onValueChange={this.onExpiryMonthChange.bind(this)}>
            <Picker.Item label={'Jan'} value={'01'} />
            <Picker.Item label={'Feb'} value={'02'} />
            <Picker.Item label={'Mar'} value={'03'} />
            <Picker.Item label={'Apr'} value={'04'} />
            
            <Picker.Item label={'May'} value={'05'} />
            <Picker.Item label={'Jun'} value={'06'} />
            <Picker.Item label={'Jul'} value={'07'} />
            <Picker.Item label={'Aug'} value={'08'} />
            
            <Picker.Item label={'Sep'} value={'09'} />
            <Picker.Item label={'Oct'} value={'10'} />
            <Picker.Item label={'Nov'} value={'11'} />
            <Picker.Item label={'Dec'} value={'12'} />
          </Picker>
           <Picker 
            style={{
              width: 100,
            }}
            selectedValue={(this.props && this.props.expyear) || '1'}
            onValueChange={this.onExpiryYearChange.bind(this)}>
            <Picker.Item label={'2017'} value={'2017'} />
            <Picker.Item label={'2018'} value={'2018'} />
            <Picker.Item label={'2019'} value={'2019'} />
            <Picker.Item label={'2020'} value={'2020'} />
            <Picker.Item label={'2021'} value={'2021'} />
            <Picker.Item label={'2022'} value={'2022'} />
          </Picker>
        </CardSection>

          <CardSection>
          <Input
            label="CVV"
            placeholder="XXX"
            onChangeText={this.onCVVChange.bind(this)}
            value={this.props.cvv}
            maxLength={3}
          />
        </CardSection>        
        
        <Text style={styles.errorTextStyle}>
          {this.props.error}
        </Text>

       <Text >
          {this.props.product}
        </Text>
        
        <CardSection>
          {this.renderButton()}
        </CardSection>
      </Card>
      </View>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 18,
    alignSelf: 'center',
    color: "rgba(255,33,0,1)"
  },
  backgroundColor: {
    backgroundColor: '#EEE', 
    flex: 0,
    height: 200
  },
  thumbnailContainerStyle: {
      justifyContent: 'center',
      alignItems: 'center',

  },
  imageStyle: {
      height: 300,
      flex: 1,
      width: null
  }
};

const mapStateToProps = ({ auth, card }) => {
  const { apikey } = auth;
  const { cardnumber, expmonth, expyear, cvv, error, loading, paymentinfo } = card; 
  return { cardnumber, expmonth, expyear, cvv, error, loading, paymentinfo, apikey };
};

export default connect(mapStateToProps, {
  cardnumberChanged, expmonthChanged, 
  expyearChanged, cvvChanged, 
  generatePaymentToken, getKey
})(AddCardForm);
