import React, { Component } from 'react';
import { View, Text, Image, TouchableWithoutFeedback } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Card, CardSection, Button } from './common';

class ListItem extends Component {

onRowPress() {
  Actions.productDetails({ product: this.props.product });
}

renderRating(rating) {
  let retStr = '';
  for (let i = 0; i < rating; i++) {
      retStr += ' \u2606 ';
  }
  return retStr;
}

  render() {
    const { id, name, rating, image_url } = this.props.product;
    const { headerContentStyle,			
            thumbnailContainerStyle,
            headerTextStyle,
            imageStyle,
            ratingStyle
          } = styles;

    return (
      <TouchableWithoutFeedback onPress={this.onRowPress.bind(this)} >
        <Card key={id}>
          <CardSection style={thumbnailContainerStyle}> 
            <Image 
            style={imageStyle}
            source={{ uri: image_url }} 
            />
          </CardSection>

          <CardSection>
              <View style={headerContentStyle}>
               <Text style={headerTextStyle}> {name} 
                 <Text style={ratingStyle}> {this.renderRating(rating)} </Text>
                </Text>
              </View>
            </CardSection>

          <CardSection> 
            <Button onPress={this.onRowPress.bind(this)} >
                view details
              </Button>
          </CardSection>
      </Card>
    </TouchableWithoutFeedback>
    );
  }
}

const styles = {
  titleStyle: {
    fontSize: 22,
    fontWeight: 'bold',
    paddingLeft: 15,
    justifyContent: 'center',
		alignItems: 'center',
  },
	headerContentStyle: {
		flexDirection: 'column',
		justifyContent: 'center',
	},
	imageStyle: {
		height: 160,
		flex: 1,
		width: null
	},
	thumbnailContainerStyle: {
		justifyContent: 'center',
		alignItems: 'center',
		marginLeft: 10,
		marginRight: 10,
        padding: 10
	},
	headerTextStyle: {
		fontSize: 18,
        fontWeight: 'bold',
        color: "rgba(106,174,25,1)",
        textAlign: 'center',
        paddingLeft:10
	},
    ratingStyle: {
    color: "rgba(234,61,17,1)"
  }
};

export default ListItem;
