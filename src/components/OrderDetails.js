import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  Picker,
  AsyncStorage
} from 'react-native'
import { connect } from 'react-redux';
import {  quantityChanged, 
          onCardNumberChange, 
          placeOrder, 
          getKey, 
          generatePaymentToken,
          fetchUserPaymentInfo,
          makeOrderPayment
       } from '../actions';
import { Card, CardSection, Input, Button, Spinner } from './common';
import { Actions } from 'react-native-router-flux';

class OrderDetails extends Component {
  componentDidMount(){
    this.props.getKey();
     AsyncStorage.getItem('username', (err, username) => {
      this.props.fetchUserPaymentInfo(username);
    });  
  }
  
  onQuantityChange(text) {
    this.props.quantityChanged(text);
  }

  onCardNumberChange(text) {
    this.props.cardnumberChanged(text);
  }
  
  onMakePaymentButtonPress() {
    const { quantity, apikey } = this.props;
    const { stripe_cust_id, last4} = this.props.paymentinfo[0];
    const { id, name, cost, image_url } = this.props.product;
    const amount = (quantity * cost).toFixed(2);
    AsyncStorage.getItem('username', (err, username) => {
        this.props.makeOrderPayment({ apikey, username, quantity, 
                                      amount, name, stripe_cust_id});
    });  
  }
  
  renderPlaceOrderButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
    
    return (
      <Button onPress={this.onMakePaymentButtonPress.bind(this)}>
        Place Order
      </Button>
    );
  }

  onAddCardButtonPress(){
    Actions.addCardDetails(this.props.product);
  }

  
  renderAddCardButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
    
    return (
      <Button onPress={this.onAddCardButtonPress.bind(this)}>
        Add / Update Card
      </Button>
    );
  }
  
  renderCardInfoSection(notificationBoxStyle, notificationTextStyle, totalCostStyle, cost) {
    
    if (typeof this.props.paymentinfo !== 'undefined') {
      const [{last4}] = this.props.paymentinfo;      
      if( last4 !== null)
      {
        console.log("INSIDE IF");
        return (
              <View>
                    <CardSection>
                       <Text> Quantity </Text>
                      <Picker 
                        style={{
                          width: 200,
                        }}
                        selectedValue={(this.props && this.props.quantity) || '1'}
                        onValueChange={this.onQuantityChange.bind(this)}>
                        <Picker.Item label={'1'} value={'1'} />
                        <Picker.Item label={'3'} value={'3'} />
                        <Picker.Item label={'6'} value={'6'} />
                        <Picker.Item label={'10'} value={'10'} />
                      </Picker>
                    </CardSection>

                    <CardSection style={{justifyContent: 'flex-end'}}>
                      <Text style={totalCostStyle}>
                         Total Cost is &nbsp; &nbsp; 
                        <Text style={{color:  "rgba(39,127,231,1)" }}>
                          ${ (((this.props && this.props.quantity) || '1') * cost).toFixed(2)} 
                        </Text>
                      </Text>
                    </CardSection>
            
                  <CardSection>
                      <Input
                        label="Card ending with"
                        placeholder="XXXX"
                        value={last4}
                        maxLength={4}
                        editable={0}
                      />
                    </CardSection>

                    <CardSection>
                      {this.renderPlaceOrderButton()}
                    </CardSection>
              </View>
          );
      }
      else
      {
                console.log("INSIDE ELSE");

          return (
            <View>
              <View style={notificationBoxStyle}>
               <Text style={notificationTextStyle} numberOfLines={3}> 
                 Please use the button below to add a credit card to your account. 
                </Text>
               </View>
             {this.renderAddCardButton()}
            </View>
            );
      }
     
    }

  }
  
  
  render() {
    
    const { id, name, cost, image_url } = this.props.product;
      const { headerContentStyle,			
            thumbnailContainerStyle,
            thumbnailStyle,
            headerTextStyle,
            imageStyle,
            backgroundColor,
            errorTextStyle,
            totalCostStyle,
            notificationBoxStyle,
            notificationTextStyle
          } = styles;

    return (
      <View style={backgroundColor} >
       <Card>
        <CardSection>
            <Text> Selected Smoothie</Text>
        </CardSection>
      </Card>

         <CardSection style={thumbnailContainerStyle}> 
            <Image 
            style={thumbnailStyle}
            source={{ uri: image_url }} 
            />
              <View style={headerContentStyle}>
                <Text style={headerTextStyle}> {name} </Text>
                <Text style={totalCostStyle}> ${cost}  </Text>
              </View>
            </CardSection>
      <Card>
        
        {this.renderCardInfoSection(notificationBoxStyle, notificationTextStyle, totalCostStyle, cost)}
        
        <Text style={errorTextStyle}>
          {this.props.error}
        </Text>
      </Card>
      </View>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  },
  backgroundColor: {
    backgroundColor: '#EEE', 
    flex: 0.3,
    height: 600
  },
  thumbnailStyle: {
		height: 90,
		width: 90
	},
	thumbnailContainerStyle: {
		justifyContent: 'center',
		alignItems: 'center',
		marginLeft: 10,
		marginRight: 10

	},
	headerTextStyle: {
      fontSize: 18,
      fontWeight: 'bold'
	},
     totalCostStyle: {
      fontWeight: 'bold', 
      color: "rgb(103,103,103)",
      fontSize: 20
     },
      notificationBoxStyle: {
		flexDirection: 'column',
		justifyContent: 'space-around',
        padding: 10,
	},  
  notificationTextStyle: {
    fontSize: 18,
    alignSelf: 'center',
    color: "rgba(200,60,60,1)",
    flex: 1,
    textAlign: 'justify'
  },  
};

const mapStateToProps = ({auth, product, card}) => {
    
  const { apikey } = auth;
  const { quantity } = product;
  const { paymentinfo, loading } = card;
  return { apikey, quantity,  loading, paymentinfo  };
};

export default connect(mapStateToProps, {
 quantityChanged, onCardNumberChange, placeOrder, getKey, generatePaymentToken, fetchUserPaymentInfo, makeOrderPayment
})(OrderDetails);
