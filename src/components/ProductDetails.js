import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, CardSection, Button } from './common';
import { View, Text, Image, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';

class ProductDetails extends Component {

  onButtonPress() {
    Actions.orderDetails({ product: this.props.product });
  }

  renderRating(rating) {
    let retStr = '';
    for (let i = 0; i < rating; i++) {
        retStr += ' \u2606 ';
    }
    return retStr;
  }

  renderIngredients(ingredients, ingredientsListItem) {
      let i = 0;
      ingredients = ` ${ingredients}`;
      const displayList = ingredients.split('##').map((item) => {
        i++;
        return <Text style={ingredientsListItem} key={i} numberOfLines={1}>{"\u2022"}  {item} </Text>;
      });

    return displayList;
  }

  renderPreparation(instructions) {
    return instructions;
  }


  render() {
    const { id, name, rating, image_url, ingredients, 
           instructions, preptime, cost, comments } = this.props.product;
    const { headerContentStyle,			
            thumbnailContainerStyle,
            headerTextStyle,
            ingredientsBoxStyle,
            ingredientsInfo,
            ingredientsHeading,
            imageStyle,
            scrollView,
            ingredientsListItem,
            ratingStyle,
            costStyle
          } = styles;

    return (
        <View>
        <ScrollView
                  automaticallyAdjustContentInsets={false}
                  onScroll={() => { console.log('onScroll!'); }}
                  scrollEventThrottle={200}
                  style={scrollView}
        >
        <Card>
          <CardSection>
              <View style={headerContentStyle}>
                <Text style={headerTextStyle}> {name} 
                  <Text style={costStyle}> ${cost} </Text>
                </Text>
                <Text style={ratingStyle}> {this.renderRating(rating)} </Text>
                <Text style={ingredientsInfo}> Preparation time {preptime} minutes </Text>
              </View>
            </CardSection>
      
          <CardSection style={thumbnailContainerStyle}> 
            <Image 
            style={imageStyle}
            source={{ uri: image_url }} 
            />
          </CardSection>

              <View style={ingredientsBoxStyle}>              
                <Text style={ingredientsHeading}>Ingredients </Text>
                {this.renderIngredients(ingredients, ingredientsListItem)}
              </View>
            
              <View style={ingredientsBoxStyle}>
                <Text style={ingredientsHeading}> Nutrition Info</Text>
                <Text style={ingredientsInfo} numberOfLines={4}
                > {comments} </Text>
              </View>
          
            <View style={ingredientsBoxStyle}>
                <Text style={ingredientsHeading}> Summary </Text>
                <Text style={ingredientsInfo} numberOfLines={12}
                > {this.renderPreparation(instructions)} </Text>
              </View>
            
          <CardSection> 
            <Button onPress={this.onButtonPress.bind(this)} >
                buy now
              </Button>
          </CardSection>
      </Card>
      </ScrollView>
      </View>
    );
  }
}

const styles = {
  titleStyle: {
    fontSize: 18,
    paddingLeft: 15,
    justifyContent: 'center',
		alignItems: 'center',
  },
	headerContentStyle: {
		flexDirection: 'column',
		justifyContent: 'space-around',
	},
    ingredientsBoxStyle: {
		flexDirection: 'column',
		justifyContent: 'space-around',
        padding: 10
	},
	imageStyle: {
		height: 200,
		flex: 1,
		width: null

	},
	thumbnailStyle: {
		height: 50,
		width: 50

	},
	thumbnailContainerStyle: {
		justifyContent: 'center',
		alignItems: 'center',
		marginLeft: 10,
		marginRight: 10

	},
	headerTextStyle: {
		fontSize: 18,
        fontWeight: 'bold',
        color: "rgba(106,174,25,1)",
        textAlign: 'center',
        paddingRight:20
	},
    ratingStyle: {
      color: "rgba(234,61,17,1)"
    },
    scrollView: {
      height: 600,
    },
    ingredientsInfo: {
      fontWeight: '500', 
      color: "rgba(21,62,10,1)", 
      flex: 1,
      textAlign: 'justify'
    },
     ingredientsHeading: {
      fontWeight: 'bold', 
      color: "rgba(116,177,104,1)",
      fontSize: 20
     },
    ingredientsListItem: {
      fontWeight: 'bold'
    },
    costStyle: {
       fontWeight: 'bold',
       color: "rgba(9,100,225,1)",      
    }
};

const mapStateToProps = state => {
  return { item: state.product.item };
};

export default connect(mapStateToProps, null)(ProductDetails);
