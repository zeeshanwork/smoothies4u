import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { emailChanged, passwordChanged, loginUser } from '../actions';
import { Card, CardSection, Input, Button, Spinner } from './common';
import { Actions } from 'react-native-router-flux';

class LoginForm extends Component {
  onEmailChange(text) {
    this.props.emailChanged(text);
  }

  onPasswordChange(text) {
    this.props.passwordChanged(text);
  }

  onButtonPress() {
    const { email, password } = this.props;
    AsyncStorage.setItem('username', email); 
    this.props.loginUser({ email, password });
  }

  renderButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button onPress={this.onButtonPress.bind(this)}>
        Login
      </Button>
    );
  }

  openSignupPage(){
      Actions.signup();
  }

  render() {
    return (
      <View style={styles.backgroundColor} >
       <Card>
          <CardSection style={styles.thumbnailContainerStyle}> 
            <Image 
            style={styles.imageStyle}
            source={ require('../assets/smoothiesgrid.jpg')} 
            />
        </CardSection>
      </Card>
        
      <Card>
        <CardSection>
          <Input
            label="Email"
            placeholder="email@gmail.com"
            onChangeText={this.onEmailChange.bind(this)}
            value={this.props.email}
          />
        </CardSection>

        <CardSection>
          <Input
            secureTextEntry
            label="Password"
            placeholder="password"
            onChangeText={this.onPasswordChange.bind(this)}
            value={this.props.password}
          />
        </CardSection>

        <Text style={styles.errorTextStyle}>
          {this.props.error}
        </Text>

        <CardSection>
          {this.renderButton()}
        </CardSection>

         <TouchableOpacity
            onPress={this.openSignupPage.bind(this)}>
             <View>
              <Text style={styles.text}> Don't have an account <Text style={{fontWeight: 'bold', color: 'blue'}}> Signup </Text> </Text>
            </View>
          </TouchableOpacity>
      </Card>
        
      </View>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  },
  backgroundColor: {
    backgroundColor: '#EEE', 
    flex: 0.3,
    height: 200
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 10,
    paddingTop: 30,
  },
  button: {
    padding: 10,
    backgroundColor: '#3B5998',
    marginBottom: 10,
  },
  text: {
    color: 'gray',
    alignSelf: 'center'
  },
  thumbnailContainerStyle: {
      justifyContent: 'center',
      alignItems: 'center',

  },
  imageStyle: {
      height: 300,
      flex: 1,
      width: null
  }
};

const mapStateToProps = ({ auth }) => {
  const { email, password, error, loading } = auth;

  return { email, password, error, loading };
};

export default connect(mapStateToProps, {
  emailChanged, passwordChanged, loginUser
})(LoginForm);
