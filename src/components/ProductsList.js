import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ListView, Text, View, Image, Linking } from 'react-native';
import { productsFetch } from '../actions';
import ListItem from './ListItem';

class ProductsList extends Component {
  componentWillMount() {
    this.props.productsFetch();
    this.createDataSource(this.props.list);
  }

  componentWillReceiveProps(nextProps) {  
    this.createDataSource(nextProps.list);
  }

  createDataSource(products) {    
    const ds = new ListView.DataSource({
       rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.dataSource = ds.cloneWithRows(products);
  }

 renderRow(product) {
   return (<ListItem product={product} />);
 }

  render() {  
    console.log(this.props);    
      return (
        <ListView
             enableEmptySections
             dataSource={this.dataSource}
             renderRow={this.renderRow}
        />
      );
  }
}
const mapStateToProps = state => {
  return { list: state.product.list };
};

export default connect(mapStateToProps, { productsFetch })(ProductsList);
