import React from 'react';
import { Scene, Router, Actions } from 'react-native-router-flux';
import LoginForm from './components/LoginForm';
import SignupForm from './components/SignupForm';

import ProductsList from './components/ProductsList';
import ProductDetails from './components/ProductDetails';
import OrderDetails from './components/OrderDetails';
import AddCardInfo from './components/AddCardInfo';
import Order from './components/Order';

const RouterComponent = () => {
  return (
    <Router sceneStyle={{ paddingTop: 65 }}>
       
      <Scene key="auth">     
          <Scene 
          key="signup" 
          component={SignupForm} 
          title="Sign up" />
        
        <Scene 
          key="login" 
          component={LoginForm} 
          title="Sign in" 
          initial />
      </Scene>    

      
      <Scene key="main">
        <Scene
          onRight={() => Actions.auth()}
          rightTitle="Logout"
          key="productsList"
          component={ProductsList}
          title="Our Products"
          initial
           />
        
        <Scene 
          key="productDetails" 
          onRight={() => Actions.auth()}
          rightTitle="Logout"
          component={ProductDetails} 
          title="Smoothie Details" />
        
        <Scene 
          key="orderDetails" 
          onRight={() => Actions.auth()}
          rightTitle="Logout"
          component={OrderDetails} 
          title="Order Details" />
  
           <Scene 
          key="addCardDetails" 
          onRight={() => Actions.auth()}
          rightTitle="Logout"
          component={AddCardInfo} 
          title="Add Card Details" 
            />
        
           <Scene 
          key="order" 
          onRight={() => Actions.auth()}
          rightTitle="Logout"
          component={Order} 
          title="Order" 
            />
        </Scene>
                   
    </Router>
  );
};

export default RouterComponent;
